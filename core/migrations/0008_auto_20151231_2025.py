# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2015-12-31 19:25
from __future__ import unicode_literals

import core.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_auto_20151231_1923'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='appointmentlogs',
            name='appointment',
        ),
        migrations.AddField(
            model_name='appointmentlogs',
            name='appointment',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name=core.models.Appointments),
        ),
    ]
